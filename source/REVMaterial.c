/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/
#include "REV.h"

GXTexObj * CargarTextura(const char* filename)
{
	GXTexObj * texObj1 = (GXTexObj*)memalign(32, sizeof(GXTexObj));
	void * texture_data1 = NULL;
	PNGUPROP imgProp;
	IMGCTX ctx;
	ctx = PNGU_SelectImageFromDevice (filename);
	PNGU_GetImageProperties (ctx, &imgProp);
	texture_data1 = memalign (32, imgProp.imgWidth * imgProp.imgHeight * 4);
	GX_InitTexObj (texObj1, texture_data1, imgProp.imgWidth, imgProp.imgHeight, GX_TF_RGBA8, GX_CLAMP, GX_CLAMP, GX_FALSE);
	PNGU_DecodeTo4x4RGBA8 (ctx, imgProp.imgWidth, imgProp.imgHeight, texture_data1, 255);
	PNGU_ReleaseImageContext (ctx);
	DCFlushRange (texture_data1, imgProp.imgWidth * imgProp.imgHeight * 4);
	return texObj1;
}

struct REV_Material * NewMat(GXTexObj * Textura, u8 Color, u16 ID)
{
	struct REV_Material * TMat;
	TMat = (struct REV_Material *)malloc(sizeof(struct REV_Material));
	TMat->Textura = Textura;
	TMat->Color = Color;
	TMat->Identifier = ID;
	return TMat;
}

struct REV_Material * MatFromImg(const char* filename, u8 Color, u16 ID)
{
	return NewMat(CargarTextura(filename), Color, ID);	
}
