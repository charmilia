/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/
#include "REV.h"

#define TEMP_SIZE     65536
#define MAXLINE 60
#define LoaderVersion 0.1

u8 REV_DefaultColor = 0;

f32 VtsPlano[] ATTRIBUTE_ALIGN(32) =//Los cuatro vertices de un plano
{
	300.0f, -300.0f, 0.0f,//0
	-300.0f, -300.0f, 0.0f,//1
	-300.0f, 300.0f, 0.0f,//2
	300.0f, 300.0f, 0.0f,//3
};

f32 NmsPlano[] ATTRIBUTE_ALIGN(32) =//Los cuatro vertices de un plano
{
	0.0f, 0.0f, 1.0f,//0
};

f32 CoordsPlano[] ATTRIBUTE_ALIGN(32) =
{
	0.0f, 0.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
	0.0f, 1.0f,
};

void REV_SetDefCLR(u8 Color)
{
	REV_DefaultColor = Color;
}

MESH * Plano(u16 Identifier)
{
	MESH * Plano = (MESH*)memalign(0x40, sizeof(MESH));
	Plano->Visible = 1;
	Plano->Identifier = Identifier;
	Plano->Vertices = VtsPlano;
	Plano->Coordenadas = CoordsPlano;
	Plano->NVerts = 4;
	Plano->NFaces = 2;
	Plano->Ncoords = 4;
	Plano->NNormals = 1;
	Plano->Normales = NmsPlano;
	Plano->VList = (u16*)malloc(6*sizeof(u16));
	Plano->NList = (u16*)malloc(6*sizeof(u16));
	Plano->TList = (u16*)malloc(6*sizeof(u16));
	Plano->VList[0] = 0;
	Plano->VList[1] = 1;
	Plano->VList[2] = 2;
	Plano->VList[3] = 2;
	Plano->VList[4] = 3;
	Plano->VList[5] = 0;
	Plano->NList[0] = 0;
	Plano->NList[1] = 0;
	Plano->NList[2] = 0;
	Plano->NList[3] = 0;
	Plano->NList[4] = 0;
	Plano->NList[5] = 0;
	Plano->TList[0] = 0;
	Plano->TList[1] = 1;
	Plano->TList[2] = 2;
	Plano->TList[3] = 2;
	Plano->TList[4] = 3;
	Plano->TList[5] = 0;
	return Plano;
}

MESH * AbrirMesh(const char * filename)
{
	MESH * Nuevo = (MESH*)memalign(0x40,sizeof(MESH));
	Nuevo->Visible  = 1;
	MESH * Aux = Plano(5);
	if(!Nuevo) return NULL;
	FILE * modelfile = fopen(filename, "rt");
	if(!modelfile){
		free(Nuevo);
		return NULL;
	}
	char linebuffer[MAXLINE];
	char * datatype;
	f32 version = 0;
	u16 verts = 0, texts = 0, normals = 0, faces = 0;
	
	Nuevo->NVerts = 0;
	Nuevo->NNormals = 0;
	Nuevo->Ncoords = 0;
	Nuevo->NFaces = 0;
	
	//Una vez declarado todo lo que vamos a usar empezamos a explorar el archivo.
	while(!feof(modelfile))
	{
		fgets(linebuffer, MAXLINE, modelfile);
		if(!feof(modelfile))
		{
			datatype = strtok(linebuffer," ");
			if(datatype[0] != '#')//Si una linea empieza por # es que es un comentario, por tanto no se procesa
			{//En caso contrario
				if(datatype[0] == 'm')//Estamos ante la informacion de version de archivo
				{
					version = atof(strtok(NULL, " "));
				}
				if(datatype[0] == 'q')
				{
					if(datatype[1] == 'v')
					{
						Nuevo->NVerts = (u16)atoi(strtok(NULL, " "));
						Nuevo->Vertices = (f32*)memalign(32, sizeof(f32)*3*Nuevo->NVerts);
					}
					if(datatype[1] == 'n')
					{
						Nuevo->NNormals = (u16)atoi(strtok(NULL, " "));
						Nuevo->Normales = (f32*)memalign(32, sizeof(f32)*3*Nuevo->NNormals);
					}
					if(datatype[1] == 't')
					{
						Nuevo->Ncoords = (u16)atoi(strtok(NULL, " "));
						Nuevo->Coordenadas = (f32*)memalign(32, sizeof(f32)*2*Nuevo->Ncoords);
					}
					if(datatype[1] == 'f')
					{
						Nuevo->NFaces = (u16)atoi(strtok(NULL, " "));
						Nuevo->VList = (u16*)malloc(sizeof(u16)*3*Nuevo->NFaces);
						Nuevo->NList = (u16*)malloc(sizeof(u16)*3*Nuevo->NFaces);
						Nuevo->TList = (u16*)malloc(sizeof(u16)*3*Nuevo->NFaces);
					}
				}
				if(datatype[0] == 'v')
				{if(verts < Nuevo->NVerts){
					Nuevo->Vertices[verts*3] = (f32)atof(strtok(NULL, " "));
					Nuevo->Vertices[verts*3+1] = (f32)atof(strtok(NULL, " "));
					Nuevo->Vertices[verts*3+2] = (f32)atof(strtok(NULL, " "));
					verts ++;
				}}
				if(datatype[0] == 'n')
				{if(normals < Nuevo->NNormals){
					Nuevo->Normales[normals*3] = (f32)atof(strtok(NULL, " "));
					Nuevo->Normales[normals*3+1] = (f32)atof(strtok(NULL, " "));
					Nuevo->Normales[normals*3+2] = (f32)atof(strtok(NULL, " "));
					normals ++;
				}}
				if(datatype[0] == 't')
				{ if(texts < Nuevo->Ncoords){
					Nuevo->Coordenadas[texts*2] = (f32)atof(strtok(NULL, " "));
					Nuevo->Coordenadas[texts*2+1] = (f32)atof(strtok(NULL, " "));
					texts ++;
				}}
				if(datatype[0] == 'f')
				{if(faces < Nuevo->NFaces){
					Nuevo->VList[faces*3] = (u16)atoi(strtok(NULL, "/"));
					Nuevo->NList[faces*3] = (u16)atoi(strtok(NULL, "/"));
					Nuevo->TList[faces*3] = (u16)atoi(strtok(NULL, " "));
					
					Nuevo->VList[faces*3+1] = (u16)atoi(strtok(NULL, "/"));
					Nuevo->NList[faces*3+1] = (u16)atoi(strtok(NULL, "/"));
					Nuevo->TList[faces*3+1] = (u16)atoi(strtok(NULL, " "));
					
					Nuevo->VList[faces*3+2] = (u16)atoi(strtok(NULL, "/"));
					Nuevo->NList[faces*3+2] = (u16)atoi(strtok(NULL, "/"));
					Nuevo->TList[faces*3+2] = (u16)atoi(strtok(NULL, " "));
					faces++;
				}}
			}
		}
	}//////*/
	fclose(modelfile);
	Nuevo->Identifier = Aux->Identifier;
	return Nuevo;
}
