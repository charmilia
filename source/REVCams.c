/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/
#include "REV.h"


void UpdtCam(CAMERA Camera, Mtx View)
{
	guMtxIdentity(View);
	guLookAt(View, &(Camera.cam), &(Camera.up), &(Camera.look));
}

void MoveCam(CAMERA * Camera, f32 front,f32  side,f32 up)
{
	Vector pos, look, tlook, sideV;
	tlook.x = Camera->look.x - Camera->cam.x;
	tlook.y = Camera->look.y - Camera->cam.y;
	tlook.z = Camera->look.z - Camera->cam.z;
	c_guVecCross(&Camera->up,&tlook,&sideV);
	pos.x = Camera->cam.x + tlook.x * front + Camera->up.x * up + side * sideV.x;
	pos.y = Camera->cam.y + tlook.y * front + Camera->up.y * up + side * sideV.y;
	pos.z = Camera->cam.z + tlook.z * front + Camera->up.z * up + side * sideV.z;
	look.x = Camera->look.x + tlook.x * front + Camera->up.x * up + side * sideV.x;
	look.y = Camera->look.y + tlook.y * front + Camera->up.y * up + side * sideV.y;
	look.z = Camera->look.z + tlook.z * front + Camera->up.z * up + side * sideV.z;
	Camera->cam = pos;
	Camera->look = look;
}

void RotateCamByAxis(CAMERA * Camera, f32 x, f32 y, f32 z, f32 Angle)
{
	Vector look, nlook;
	look.x = Camera->look.x - Camera->cam.x;
	look.y = Camera->look.y - Camera->cam.y;
	look.z = Camera->look.z - Camera->cam.z;
	f32 costheta = cosf(Angle);
	f32 sintheta = sinf(Angle);
	
	//////////
	nlook.x = (costheta + (1 - costheta)*x*x) * look.x;
	nlook.x += ((1-costheta)*x*y-z*sintheta) * look.y;
	nlook.x+=((1-costheta)*x*z+y*sintheta)*look.z;
	/////////
	
	nlook.y = (costheta + (1 - costheta)*y*y) * look.y;
	nlook.y += ((1-costheta)*y*z-x*sintheta) * look.z;
	nlook.y+=((1-costheta)*x*y+z*sintheta)*look.x;
	
	nlook.z = (costheta + (1 - costheta)*z*z) * look.z;
	nlook.z += ((1-costheta)*x*z-y*sintheta) * look.x;
	nlook.z+=((1-costheta)*y*z+x*sintheta)*look.y;

	//////////
	Camera->look.x = Camera->cam.x + nlook.x;
	Camera->look.y = Camera->cam.y + nlook.y;
	Camera->look.z = Camera->cam.z + nlook.z;
}

void RotateCam(CAMERA * Camera, f32 pan, f32 tilt, f32 roll)
{
	Vector look, side;
	look.x = Camera->look.x - Camera->cam.x;
	look.y = Camera->look.y - Camera->cam.y;
	look.z = Camera->look.z - Camera->cam.z;
	c_guVecCross(&Camera->up,&look,&side);
	RotateCamByAxis(Camera, Camera->up.x, Camera->up.y, Camera->up.z, pan);
	RotateCamByAxis(Camera, side.x, side.y, side.z, tilt);
	RotateCamByAxis(Camera, look.x, look.y, look.z, roll);
}
