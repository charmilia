/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/
#include "REV.h"

static void *frameBuffer[2] = { NULL, NULL};
GXRModeObj *rmode;
Mtx view; // view and perspective matrices
Mtx model, modelview;

u32	fb = 0; 	// initial framebuffer index
GXColor background = {0, 0, 0, 0xff};

Vector axis = {0,0,1};

GXColor AmbientColor;

GXLightObj LuzEspecular;//Para la iluminacion especular.

f32 Vertices[] ATTRIBUTE_ALIGN(32) =//los 8 vertices de un cubo
{
	 10.0f, 10.0f,	10.0f,	// 0
	-10.0f, 10.0f,	10.0f,	// 1
	-10.0f,	10.0f,	-10.0f, // 2
	 10.0f,	10.0f,	-10.0f, // 3
	 10.0f, -10.0f,	10.0f,	// 0
	-10.0f, -10.0f,	10.0f,	// 1
	-10.0f,	-10.0f,	-10.0f, // 2
	 10.0f,	-10.0f,	-10.0f, // 3
};

f32 Normales[] ATTRIBUTE_ALIGN(32) =//los 8 Normals de un cubo
{
	 10.0f, 10.0f,	10.0f,	// 0
	-10.0f, 10.0f,	10.0f,	// 1
	-10.0f,	10.0f,	-10.0f, // 2
	 10.0f,	10.0f,	-10.0f, // 3
	 10.0f, -10.0f,	10.0f,	// 0
	-10.0f, -10.0f,	10.0f,	// 1
	-10.0f,	-10.0f,	-10.0f, // 2
	 10.0f,	-10.0f,	-10.0f, // 3
};

u8 Colores[] ATTRIBUTE_ALIGN(32) =//Colores predefnidos
{
	0,	0,	0,	255, // 00 Negro
	255,255,255,255, // 01 Blanco
	255,0,	 0,	255, // 02 Rojo
	0	,255,0 ,255, // 03 Verde
	0,	0,	255,50, // 04 Azul
	255,255,0,255, // 05 Amarillo
	255,0,	255,255, // 06 Morado
	0,255,	255,255, // 07 Aqua
	255,127,0,	255, // 08 Naranja
	127,127,127,255, // 09 Gris
	63,63,63,255,    // 10 Gris Oscuro
	200,200,200,255, // 11 Gris Claro
};

f32 Coordenadas[] ATTRIBUTE_ALIGN(32) =//coordenadas de texturas predefinidas
{
	0.0f,0.0f,
	1.0f,0.0f,
	1.0f,1.0f,
	0.0f,1.0f,
};

void REV_Init()
{
	Mtx perspective;
	
	f32 yscale;
	u32 xfbHeight;
	
	u32	fb = 0; 	// initial framebuffer index
	GXColor background = {0, 0, 0, 0xff};
	
	rmode = VIDEO_GetPreferredMode(NULL);
	
	// allocate 2 framebuffers for double buffering
	frameBuffer[0] = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));
	frameBuffer[1] = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));

	VIDEO_Configure(rmode);
	VIDEO_SetNextFramebuffer(frameBuffer[fb]);
	VIDEO_SetBlack(FALSE);
	VIDEO_Flush();
	VIDEO_WaitVSync();
	if(rmode->viTVMode&VI_NON_INTERLACE) VIDEO_WaitVSync();

	//fat init default
	fatInitDefault ();
	// setup the fifo and then init the flipper
	void *gp_fifo = NULL;
	gp_fifo = memalign(32,DEFAULT_FIFO_SIZE);
	memset(gp_fifo,0,DEFAULT_FIFO_SIZE);
 
	GX_Init(gp_fifo,DEFAULT_FIFO_SIZE);
 
	// clears the bg to color and clears the z buffer
	GX_SetCopyClear(background, 0x00ffffff);
	
	GX_SetViewport(0,0,rmode->fbWidth,rmode->efbHeight,0,1);
	yscale = GX_GetYScaleFactor(rmode->efbHeight,rmode->xfbHeight);
	xfbHeight = GX_SetDispCopyYScale(yscale);
	GX_SetScissor(0,0,rmode->fbWidth,rmode->efbHeight);
	GX_SetDispCopySrc(0,0,rmode->fbWidth,rmode->efbHeight);
	GX_SetDispCopyDst(rmode->fbWidth,xfbHeight);
	GX_SetCopyFilter(rmode->aa,rmode->sample_pattern,GX_TRUE,rmode->vfilter);
	GX_SetFieldMode(rmode->field_rendering,((rmode->viHeight==2*rmode->xfbHeight)?GX_ENABLE:GX_DISABLE));
 
	GX_SetCullMode(GX_CULL_NONE);
	GX_CopyDisp(frameBuffer[fb],GX_TRUE);
	GX_SetDispCopyGamma(GX_GM_1_0);
	
	draw_init();
	
	f32 w = rmode->viWidth;
    f32 h = rmode->viHeight;
	
	guPerspective(perspective, 45, (f32)w/h, 0.1F, 1000.0F);
	GX_LoadProjectionMtx(perspective, GX_PERSPECTIVE);
	
	GX_SetViewport(0,0,rmode->fbWidth,rmode->efbHeight,0,1);
}

void draw_init()
{
	GX_ClearVtxDesc();
	GX_InvalidateTexAll();
	GX_SetVtxDesc(GX_VA_POS, GX_INDEX16);
 	GX_SetVtxDesc(GX_VA_NRM, GX_INDEX16);
 	GX_SetVtxDesc(GX_VA_CLR0, GX_INDEX16);
 	GX_SetVtxDesc(GX_VA_TEX0, GX_INDEX16);
	
	GX_SetArray(GX_VA_POS, Vertices, 3*sizeof(f32));
	GX_SetArray(GX_VA_NRM, Normales, 3*sizeof(f32));
	GX_SetArray(GX_VA_CLR0, Colores, 4*sizeof(u8));
	GX_SetArray(GX_VA_TEX0, Coordenadas, 2*sizeof(f32));
	
	GX_SetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
	GX_SetVtxAttrFmt(GX_VTXFMT0, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0);
	GX_SetVtxAttrFmt(GX_VTXFMT0, GX_VA_NRM, GX_NRM_XYZ, GX_F32, 0);
	GX_SetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_F32, 0);
	
	GX_SetNumChans(1);
	GX_SetTevOp(GX_TEVSTAGE0, GX_DECAL);
	GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
}

void REV_Process(ROOT mRoot, CAMERA * mCamara)
{
	OBJECT * Auxiliar = NULL;
	MESH * tMesh = NULL;
	MATERIAL * tMat = NULL;
	GXTexObj * textura1;
	u8 tColor = 0;
	Vector Pos = (Vector){0.0,0.0,0.0};
	Vector Ang = (Vector){0.0, 0.0, 0.0};
	u32 i=0;
	PANEL * PAux = NULL;
	UpdtCam(*mCamara, view);
	Mtx model2;
	if(mCamara){
	Auxiliar = mRoot.FstObj;
	while(Auxiliar)
	{
		if(Auxiliar->Mesh){
		tMesh = Auxiliar->Mesh;
		if(Auxiliar->Material){
		GX_SetTevOp(GX_TEVSTAGE0, GX_DECAL);
		tMat = Auxiliar->Material;
		textura1 = Auxiliar->Material->Textura;
		GX_LoadTexObj(textura1, GX_TEXMAP0);
		}
		else
		GX_SetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
		guMtxIdentity(model);
		Pos = Auxiliar->Posicion;
		Ang = Auxiliar->Angulo;
		
		guMtxRotDeg(model2, 'x', Ang.x);
		guMtxConcat(model, model2, model);
		guMtxRotDeg(model2, 'y', Ang.y);
		guMtxConcat(model, model2, model);
		guMtxRotDeg(model2, 'z', Ang.z);
		guMtxConcat(model, model2, model);
		guMtxTransApply(model,model, Pos.x, Pos.y, Pos.z);
		
		guMtxConcat(view,model,modelview);
		GX_LoadPosMtxImm(modelview, GX_PNMTX0);
		
		c_guMtxInverse(modelview,modelview);
		c_guMtxTranspose(modelview,modelview);
		GX_LoadNrmMtxImm(modelview,GX_PNMTX0);
		
		GX_SetArray(GX_VA_POS, tMesh->Vertices, 3*sizeof(f32));
		GX_SetArray(GX_VA_NRM, tMesh->Normales, 3*sizeof(f32));
		GX_SetArray(GX_VA_CLR0, Colores, 4*sizeof(u8));
		GX_SetArray(GX_VA_TEX0, tMesh->Coordenadas, 2*sizeof(f32));
		
		
		for(i=0; i<tMesh->NFaces; i++)
			{
				GX_Begin(GX_TRIANGLES, GX_VTXFMT0, 3);
				
				GX_Position1x16(tMesh->VList[i*3]);
				GX_Normal1x16(tMesh->NList[i*3]);
				GX_Color1x16(tColor);
				GX_TexCoord1x16(tMesh->TList[i*3]);
				
				GX_Position1x16(tMesh->VList[(i*3)+1]);
				GX_Normal1x16(tMesh->NList[(i*3)+1]);
				GX_Color1x16(tColor);
				GX_TexCoord1x16(tMesh->TList[(i*3)+1]);
				
				GX_Position1x16(tMesh->VList[(i*3)+2]);
				GX_Normal1x16(tMesh->NList[(i*3)+2]);
				GX_Color1x16(tColor);
				GX_TexCoord1x16(tMesh->TList[(i*3)+2]);
				
				GX_End();
			}
		}
		
		Auxiliar = Auxiliar->Next;
	}

	
	GX_DrawDone();}
	
	//Ahora va la implementacion del 2D, justo antes de copiar el EFB al XFB para aprovechar la conversion RGB ;)
	PAux = mRoot.FstPnl;
	while(PAux)
	{
		if((PAux->Imagen)&&(PAux->visible))
			DrawImage(PAux->Imagen, PAux->x, PAux->y);
		PAux = PAux->Next;
	}
	//Ahora copiamos el EFB al XFB
	
	fb ^= 1;		// flip framebuffer
	GX_SetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE);
	GX_SetColorUpdate(GX_TRUE);
	GX_CopyDisp(frameBuffer[fb],GX_TRUE);

	VIDEO_SetNextFramebuffer(frameBuffer[fb]);
 
	VIDEO_Flush();

	VIDEO_WaitVSync();
}
