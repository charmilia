/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/
#include "REV.h"

void rectagulo(void)
{
	u16 x, y;
	GXColor rect = {255, 0,0,127};
	for(x=150;x<250;x++)
		for(y=100;y<200;y++)
			GX_PokeARGB(x,y,rect);
}

void DrawImage(IMAGE * source, u16 x, u16 y)
{
	u16 j, k;
	u32 i;
	GXColor tColor;
	u8* datos;
	datos = (u8*)source->ImageData;
	for(j=0;j<source->Width;j++)
		for(k=0;k<source->Height;k++)
		{
			i = 4*(j+k*source->Width);
			tColor.r = datos[i];
			tColor.g = datos[i+1];
			tColor.b = datos[i+2];
			tColor.a = datos[i+3];
			/*for(j=0;j<20;j++)
				for(k=0;k<20;k++)
				{
					GX_PokeARGB(m+20*x+j,n+20*y+k,tColor);
				}*/
			GX_PokeARGB(j+x,k+y,tColor);	
		}
}

void AddPnl(ROOT * mRoot, PANEL * Panel)
{
	Panel->Next = mRoot->FstPnl;
	mRoot->FstPnl = Panel;
}

PANEL * NewPanel(IMAGE * Imagen, u16 x, u16 y, u8 layer, u8 visible)
{
	PANEL * Nuevo = (PANEL*)malloc(sizeof(PANEL));
	Nuevo->Imagen = Imagen;
	Nuevo->x = x;
	Nuevo->y = y;
	Nuevo->visible = visible;
	return Nuevo;
}
