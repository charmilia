/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/

#include "REV.h" // incluir todos los encabezamientos del motor.

ROOT * NewRoot()
{
	ROOT * mRoot;
	mRoot = (ROOT*)malloc(sizeof(ROOT));
	mRoot->FstObj = NULL;
	mRoot->FstPnl = NULL;
	return mRoot;
}

void DelRoot(ROOT * root)
{
	OBJECT * Auxiliar;
	Auxiliar = root->FstObj;
	while(Auxiliar)
	{
		root->FstObj = root->FstObj->Next;
		FreeObj(Auxiliar);
		Auxiliar = root->FstObj;
	}
	free(root);
}

void AddObj(ROOT * root, OBJECT * object)
{
	object->Next = root->FstObj;
	root->FstObj = object;
}

OBJECT * NewObj(MESH * mesh, MATERIAL * material, Vector Posicion, Vector Angulo, u16 Identifier)
{
	OBJECT * Objeto = (OBJECT*)malloc(sizeof(OBJECT));
	Objeto->Mesh = mesh;
	Objeto->Posicion = Posicion;
	Objeto->Angulo = Angulo;
	Objeto->Identifier = Identifier;
	Objeto->Material = material;
	return Objeto;
}

void DelObjByID(ROOT * Raiz, u16 Identifier)
{
	OBJECT * Auxiliar, *Auxiliar2;
	Auxiliar = Raiz->FstObj;
	while(Auxiliar)
	{
		Auxiliar2 = Auxiliar->Next;
		if(Auxiliar->Identifier == Identifier)
			FreeObj(Auxiliar);
		Auxiliar = Auxiliar2;
	}
}

void DelObj(ROOT * Raiz, OBJECT * Object)
{
	OBJECT * Auxiliar;
	Auxiliar = Raiz-> FstObj;
	if(Auxiliar == Object)
	{
		Raiz->FstObj = Object->Next;
		FreeObj(Object);
	}
	else
	{
		while(Auxiliar)
		{
			if(Auxiliar->Next == Object)
			{
				Auxiliar->Next = Object->Next;
				FreeObj(Object);
			}
			Auxiliar = Auxiliar->Next;
		}
	}
}

void FreeObj(OBJECT * Object)
{
	free(Object);
}

void clearAll(ROOT * mRoot);
