/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/
#ifndef REVSPACE_H
#define REVSPACE_H

#include "REV.h" // incluir todos los encabezamientos del motor.

typedef struct REV_Object{//Object, objeto en el espacio.

struct REV_Object * Next;//Puntero al siguiente objeto de la lista.
u16 Identifier;//Numero identificador del objeto. Varios objetos pueden tener el mismo identificador.
Vector Posicion,//La posicion que el objeto ocupa en el espacio.
		Angulo;//Su orientacion en el espacio
struct REV_Mesh * Mesh;//Color Modelo 3D
struct REV_Material * Material;//Material del modelo, con informacion de colores, alpha, etc...
} OBJECT;

typedef struct REV_Root{//Root
struct REV_Object * FstObj;//Primer objeto de la lista;
struct REV_Panel * FstPnl;
} ROOT;

//Funciones

/****** NewRoot ******
//Descripcion: Crea una nueva raiz que el motor puede utilizar, de momento no se soportan multiples raices
//Argumentos: Ninguno
//Devuelve: Puntero a la nueva raiz creada*/
struct REV_Root * NewRoot();

/****** DelRoot ******
//Descripcion: Borra una raiz y todos sus elementos, liberando la memoria que ocupaban
//Argumentos: Puntero a la raiz que se quiera borrar
//Devuelve: Nada */

void DelRoot(ROOT * root);

/****** AddObj ******
Descripcion: A�ade un objeto al comienzo de la Raiz
Argumentos: 
	Raiz: Raiz a la que se a�adira el nuevo objeto.
	Objeto: Puntero al objeto que se va a a�adir
Devuelve: Nada */

void AddObj(ROOT * root, OBJECT * object);

/****** NewObj ******
Descripcion: Crea un objeto nuevo.
Argumentos: 
	Mesh: Puntero al mesh que tendra el objeto (ver REVMesh.h)
	Posicion y orientacion: la posicion y orientacion iniciales del nuevo objeto en el espacio
	Identifier: u16 que sirve de identificador del objeto dentro de la Raiz. Puede ser comun a varios objetos.
Devuelve: Puntero al nuevo objeto */

struct REV_Object * NewObj(struct REV_Mesh * mesh, struct REV_Material * material, Vector Pos, Vector Ang, u16 Identifier);

/****** DelObjByID ******
Descripcion: Borra Todos los objetos de la Raiz que tengan el identificador dado, liberando el espacio que ocupaban en memoria
Argumentos: 
	Raiz: Raiz de la que se eliminaran los objetos.
	Identifier: Identificador de los objetos que se desean borrar.
Devuelve: Nada */

void DelObjByID(ROOT * root, u16 Identifier);

/****** DelObj ******
Descripcion: Borra el objeto designado por el puntero y lo elimina de la Raiz dada.
Argumentos: 
	Raiz: Raiz de la que se borrara el objeto.
	Objeto: Puntero al objeto que se desea borrar.
Devuelve: Nada */

void DelObj(ROOT * root, OBJECT * object);

/****** FreeObj ******
Descripcion: Libera de la memoria el objeto dado y todos sus componentes
Argumentos: Puntero al objeto que se desea liberar de la memoria
Devuelve: nada */

void FreeObj(OBJECT * object);

/****** Vtr3 ******
Descripcion: Crea un Vector3 a partir de sus componentes
Argumentos: las componentes x, z e y del vector resultante
Devuelve: Un vector3 */

void clearAll(ROOT * root);

#endif
