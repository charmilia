/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*///trabajo con camaras
#ifndef REV_CAMS_H
#define REV_CAMS_H

#include "REV.h"

//Funciones
/****** Rotate Cam ******/
//Descripcion: Crea una nueva raiz que el motor puede utilizar, de momento no se soportan multiples raices
//Argumentos: Ninguno
//Devuelve: Puntero a la nueva raiz creada//

typedef struct REV_Camera{
	Vector cam,
			up,
		  look;
}CAMERA;

void UpdtCam(struct REV_Camera Camera, Mtx View);//Esta funcion la utiliza el Engine automaticamente, no tienes que llamarla
void MoveCam(struct REV_Camera * Camera, f32 front,f32  side,f32 up);
void RotateCam(struct REV_Camera * Camera, f32 pan, f32 tilt, f32 roll);
void RotateCamByAxis(struct REV_Camera * Camera, f32 x, f32 y, f32 z, f32 Angle);
#endif
