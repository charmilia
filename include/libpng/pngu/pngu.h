/********************************************************************************************

PNGU Version : 0.2a

Coder : frontier

More info : http://frontier-dev.net

********************************************************************************************/
#ifndef __PNGU__
#define __PNGU__


// Return codes
#define PNGU_OK							0
#define PNGU_ODD_WIDTH					1
#define PNGU_ODD_STRIDE					2
#define PNGU_INVALID_WIDTH_OR_HEIGHT	3
#define PNGU_FILE_IS_NOT_PNG			4
#define PNGU_UNSUPPORTED_COLOR_TYPE		5
#define PNGU_NO_FILE_SELECTED			6
#define PNGU_CANT_OPEN_FILE				7
#define PNGU_CANT_READ_FILE				8
#define PNGU_LIB_ERROR					9


// Color types
#define PNGU_COLOR_TYPE_GRAY			1
#define PNGU_COLOR_TYPE_GRAY_ALPHA		2
#define PNGU_COLOR_TYPE_PALETTE			3
#define PNGU_COLOR_TYPE_RGB				4
#define PNGU_COLOR_TYPE_RGB_ALPHA		5
#define PNGU_COLOR_TYPE_UNKNOWN 		6


#ifdef __cplusplus
	extern "C" {
#endif


// Types
typedef unsigned char PNGU_u8;
typedef unsigned short PNGU_u16;
typedef unsigned int PNGU_u32;
typedef unsigned long long PNGU_u64;


typedef struct
{
	PNGU_u8 r;
	PNGU_u8 g;
	PNGU_u8 b;
} PNGUCOLOR;


typedef struct
{
	PNGU_u32 imgWidth;
	PNGU_u32 imgHeight;
	PNGU_u32 imgBitDepth;
	PNGU_u32 imgColorType;
	PNGU_u32 validBckgrnd;
	PNGUCOLOR bckgrnd;
	PNGU_u32 numTrans;
	PNGUCOLOR *trans;
} PNGUPROP;


struct _IMGCTX;


typedef struct _IMGCTX *IMGCTX;


// PNGU Interface //


// Macro to convert RGB8 values to RGB565
#define PNGU_RGB8_TO_RGB565(r,g,b) ( ((((PNGU_u16) r) & 0xF8U) << 8) | ((((PNGU_u16) g) & 0xFCU) << 3) | (((PNGU_u16) b) >> 3) )


// Macro to convert RGBA8 values to RGB5A3
#define PNGU_RGB8_TO_RGB5A3(r,g,b,a) ( ((PNGU_u16) (a ? 0x8000 : 0)) | ((((PNGU_u16) r) & 0xF8U) << 7) | ((((PNGU_u16) g) & 0xF8U) << 2) | (((PNGU_u16) b) >> 3) )


// Function to convert two RGB8 values to YCbYCr
PNGU_u32 PNGU_RGB8_TO_YCbYCr (PNGU_u8 r1, PNGU_u8 g1, PNGU_u8 b1, PNGU_u8 r2, PNGU_u8 g2, PNGU_u8 b2);


// Function to convert YCbYCr to two RGB8 values.
void PNGU_YCbYCr_TO_RGB8 (PNGU_u32 ycbycr, PNGU_u8 *r1, PNGU_u8 *g1, PNGU_u8 *b1, PNGU_u8 *r2, PNGU_u8 *g2, PNGU_u8 *b2);


// Selects a PNG file, previosly loaded into a buffer, and creates an image context for subsequent procesing.
IMGCTX PNGU_SelectImageFromBuffer (const void *buffer);


// Selects a PNG file, from any devoptab device, and creates an image context for subsequent procesing.
IMGCTX PNGU_SelectImageFromDevice (const char *filename);


// Frees resources associated with an image context. Always call this function when you no longer need the IMGCTX.
void PNGU_ReleaseImageContext (IMGCTX ctx);


// Retrieves info from selected PNG file, including image dimensions, color format, background and transparency colors.
int PNGU_GetImageProperties (IMGCTX ctx, PNGUPROP *fileproperties);


// Expand selected PNG file into a YCbYCr buffer. You need to specify image dimensions, 
// buffer addres and stride in pixels (buffer width minus image width)
int PNGU_DecodeToYCbYCr (IMGCTX ctx, PNGU_u32 width, PNGU_u32 height, void *buffer, PNGU_u32 stride);


// Macro for decoding an image inside a buffer at given coordinates.
#define PNGU_DECODE_TO_COORDS_YCbYCr(ctx,coordX,coordY,imgWidth,imgHeight,bufferWidth,bufferHeight,buffer)	\
																											\
		PNGU_DecodeToYCbYCr (ctx, imgWidth, imgHeight, ((void *) buffer) + (coordY) * (bufferWidth) * 2 +	\
							(coordX) * 2, (bufferWidth) - (imgWidth))


// Expand selected PNG file into a linear RGBA8 buffer. You need to specify image dimensions, 
// buffer addres, stride in pixels, and default alpha value.
// WARNING: In current implementation source alpha, if present, is discarded. Alpha is always set to 'default_alpha'.
int PNGU_DecodeToRGB565 (IMGCTX ctx, PNGU_u32 width, PNGU_u32 height, void *buffer, PNGU_u32 stride);


// Macro for decoding an image inside a buffer at given coordinates.
#define PNGU_DECODE_TO_COORDS_RGB565(ctx,coordX,coordY,imgWidth,imgHeight,bufferWidth,bufferHeight,buffer)	\
																											\
		PNGU_DecodeToRGB565 (ctx, imgWidth, imgHeight, ((void *) buffer) + (coordY) * (bufferWidth) * 2 +	\
							(coordX) * 2, (bufferWidth) - (imgWidth))


// Expand selected PNG file into a linear RGBA8 buffer. You need to specify image dimensions, 
// buffer addres, stride in pixels, and default alpha value.
// WARNING: In current implementation source alpha, if present, is discarded. Alpha is always set to 'default_alpha'.
int PNGU_DecodeToRGBA8 (IMGCTX ctx, PNGU_u32 width, PNGU_u32 height, void *buffer, PNGU_u32 stride, PNGU_u8 default_alpha);


// Expand selected PNG file into a 4x4 RGB565 buffer. Image dimensions and buffer addres need to be
// specified. You can use decoded image as is to create a GX_TF_RGB565 texture.
int PNGU_DecodeTo4x4RGB565 (IMGCTX ctx, PNGU_u32 width, PNGU_u32 height, void *buffer);


// Expand selected PNG file into a 4x4 RGB565 buffer. Image dimensions and buffer addres need to be specified.
// You can use decoded image as is to create a GX_TF_RGB5A3 texture. 
// WARNING: In current implementation source alpha, if present, is discarded. Transparency bit is always set to 1.
int PNGU_DecodeTo4x4RGB5A3 (IMGCTX ctx, PNGU_u32 width, PNGU_u32 height, void *buffer, PNGU_u8 default_alpha);


// Expand selected PNG file into a 4x4 RGBA8 buffer. Image dimensions, buffer addres and default alpha value need 
// to be specified. You can use decoded image as is to create a GX_TF_RGBA8 texture. 
// WARNING: In current implementation source alpha, if present, is discarded. Alpha is always set to 'default_alpha'.
int PNGU_DecodeTo4x4RGBA8 (IMGCTX ctx, PNGU_u32 width, PNGU_u32 height, void *buffer, PNGU_u8 default_alpha);


// Encodes an YCbYCr image in PNG format and saves it to the selected file or memory buffer.
int PNGU_EncodeFromYCbYCr (IMGCTX ctx, PNGU_u32 width, PNGU_u32 height, void *buffer, PNGU_u32 stride);


// Macro for encoding an image stored in a YCbYCr buffer at given coordinates.
#define PNGU_ENCODE_TO_COORDS_YCbYCr(ctx,coordX,coordY,imgWidth,imgHeight,bufferWidth,bufferHeight,buffer)	\
																											\
		PNGU_EncodeFromYCbYCr (ctx, imgWidth, imgHeight, ((void *) buffer) + (coordY) * (bufferWidth) * 2 +	\
							(coordX) * 2, (bufferWidth) - (imgWidth))


#ifdef __cplusplus
	}
#endif


#endif


