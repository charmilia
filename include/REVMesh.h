/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/
#ifndef REVMESH_H
#define REVMESH_H

#include "REV.h"

typedef struct REV_Mesh{//Mesh, datos de modelo 3D
u16 Identifier;
u16 NVerts;//Cuantos vertices tiene nuestro modelo
u16 NFaces;//Cuantas caras tiene nuestro modelo
u16 Ncoords;//Cuantas coordenadas de textura tiene nuestro modelo
u16 NNormals;//Cuantos normales tiene nuestro modelo.
f32 * Vertices;//Puntero al array de vertices 
f32 * Normales;
f32 * Coordenadas;//Puntero al array de coordenadas de textura
u16 * VList;//Puntero a la lista de faces, compuesta por indices de vertice y textura.
u16 * NList;
u16 * TList;
u8 Visible;
} MESH;

/****** REV_SetDefCLR ******
Descripcion: Cambia el color de base con el que se crean los objetos por defecto.
Argumentos : Nuevo color por defecto
Devuelve: Nada */

void REV_SetDefCLR(u8 Color);

MESH * Plano(u16 Identifier);

MESH * AbrirMesh(const char * filename);

#endif
