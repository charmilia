/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/

#ifndef REV_H
#define REV_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <ogcsys.h>
#include <gccore.h>
#include <math.h>
#include <wiiuse/wpad.h>//inclusiones de libogc
#include <fat.h>//Para poder trabajar con el sistema de archivos
#include "libpng/pngu/pngu.h"//Para las texturas en formato png

#include "REVSpace.h"//Para tratamiento de datos en el espacio.
#include "REVMesh.h"//Para el tratamiento de 3D
#include "REVdefs.h"//definiciones que pueden resultar utiles
#include "REVCams.h"//Para usar las funciones de camara
#include "REVPhysics.h"
#include "REVMaterial.h"
#include "REVImage.h"
#include "REVScreen.h"

#define DEFAULT_FIFO_SIZE	(256*1024)

void REV_Init();
void draw_init();
void REV_Process(ROOT mRoot, CAMERA * mCamera);

#endif
