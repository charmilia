/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/

/***Demo de Carga de Modelos***/
//En esta demo se muestra un puntero en pantalla.
//El puntero se mueve al apuntar con el wiimote a la pantalla.
//Y se oculta o muestra pulsando el boton A.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <wiiuse/wpad.h>//Hasta aqui los archivos tipicos que solemos incluir

#include "REV.h"//Esto lo incluimos para usar el Engine

int main()
{	
	VIDEO_Init();//Iniciamos Libogc
	WPAD_Init();//Iniciamos Wpad, para el mando.
	REV_Init();//Iniciamos el Revolution Engine
	WPADData *wiimote;
	WPAD_SetDataFormat(0, WPAD_FMT_BTNS_ACC_IR);//Codigo para el Wiimote, no se explicara aqui
	
	ROOT * mRoot = NewRoot();//Creamos una raiz.
	
	//El funcionamiento es muy simple
	IMAGE * Puntero = AbrirImagen("puntero.png");//Primero cargamos la imagen.
	PANEL * panel1 = NewPanel(Puntero, 320, 240, 3, 1);//Luego creamos un Panel
	//Un panel no es mas que una imagen mostrada en pantalla, pero mas adelante
	//se podran crear no solo panels, sino tambien Buttons, Windows, Scrolls, etc.
	//Toma como argumentos una IMAGE, dos f32 que representan su posicion en la pantalla
	//desde la esquina superior izquierda, un identificador y un indicador de visibilidad
	//(1 = visible, 0 = invisible), para poder ocultar nuestros panels.
	AddPnl(mRoot, panel1);//y finalmente lo mas importante, a�adimos nuestro Panel a la raiz.
	//Esto es muy importante porque el engine solo reconocera aquellas cosas que tengamos a�adidas
	//a la raiz. Si no esta en la raiz es como si no existiese aun. Esto es muy util para asegurarnos
	//de tener las cosas bien cargadas antes de usarlas.
	
	while(1) {//Ahora el bucle principal.
		
		WPAD_ScanPads();
		
		if (WPAD_ButtonsDown(0) & WPAD_BUTTON_HOME) exit(0);//Salir si pulsamos home
		
		wiimote = WPAD_Data(0);
		if(wiimote->ir.valid)
		{
			panel1->x = wiimote->ir.x; //actualizamos la posicion del puntero en X
			panel1->y = wiimote->ir.y; //actualizamos la posicion del puntero en Y
		}
		
		if(WPAD_ButtonsDown(0) & WPAD_BUTTON_A) panel1->Visible^=1;
		//Cuando pulsamos el boton A del mando ocultamos/mostramos el puntero.
		
		REV_Process(*mRoot, NULL);//llamamos al engine para que haga el trabajo por nosotros.
		//Como solo trabajamos con 2D no hemos creado camara y en su lugar ponemos un simple NULL.
		//Esto reducira mucho el tiempo de ejecucion, aumentando nuestro framerate.
	}
}
//Fin.