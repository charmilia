/////////////////////////////////////////////////
//Este archivo forma parte del Revolution Engine.
//Coded by Technik
/////////////////////////////////////////////////
/*Licencia:
El Revolution Engine se proporciona "Tal Cual", es decir, no se ofrece ningun tipo de garantia
sobre su correcto funcionamiento. El autor no se hace responsable del uso que se haga de este
software ni de modificaciones o variaciones del mismo, ni de las consecuencias de dicho uso.
No obstante se intentara dar apoyo a cualquier duda o pregunta que se tenga sobre el, sobre su
uso o sobre su funcionamiento, pero sin ofrecer garantias de ello.
Una vez has obtenido una copia de la totalidad o parte de este software, podras redistribuirlo
siempre y cuando se cite al autor original del codigo (technik) y se a�ada una nota con la
procedencia original del codigo (www.revolutiongameengine.blogspot.com a fecha de 24 de julio de 2008).
Puedes modificar este codigo tanto como quieras siempre que las modificaciones sean notificadas al autor
y/o su resultado sea un codigo fuente publico, sobre el cual el autor del codigo original (este) tendra
derecho de copia, modificacion, inclusion en proyectos, o cualquier uso de forma totalmente libre y gratuita.
No se obliga a nadie a pagar por este software.
Cualquier redistribucion del software debe llevar al menos una copia de esta licencia.
El autor se reserva el derecho a modificar esta licencia en el futuro tanto como crea conveniente.
Se permite el uso de la totalidad o parte de este software en cualquier proyecto no comercial siempre que
se respete y conserve esta licencia.
Para el uso de la totalidad o parte de este software para cualquier fin comercial o que requiera de otra
licencia distinta sera necesario ponerse en contacto con el autor del software (technik) cuya autorizacion
expresa es indispensable.
*/

/***Demo de Carga de Modelos***/
//En esta demo se carga un Modelo Estatico, un puntero, y se usa una camara.
//La camara se mueve con el puntero y la cruceta.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <wiiuse/wpad.h>//Hasta aqui los archivos tipicos que solemos incluir

#include "REV.h"//Esto lo incluimos para usar el Engine

int main()
{	
	VIDEO_Init();//Iniciamos Libogc
	WPAD_Init();//Iniciamos Wpad, para el mando.
	REV_Init();//Iniciamos el Revolution Engine
	WPADData *wiimote;
	WPAD_SetDataFormat(0, WPAD_FMT_BTNS_ACC_IR);//Codigo para el Wiimote, no se explicara aqui
	
	f32 posx = 320, posy = 240;
	
	ROOT * mRoot = NewRoot();//Creamos una raiz.
	
	MESH * WiiMesh = AbrirMesh("wii.rms");//Cargamos el modelo
	MATERIAL * WiiMat = MatFromImg("wiitextcm7.png", Blanco, 32);//Cargamos su textura
	OBJECT * WiiObj = NewObj(WiiMesh,WiiMat, (Vector){0.0, 0.0, 31.0}, (Vector){0.0, 0.0, 0.0}, 9);
	//Ni los modelos ni las texturas valen de nada si no estan enlazados y se les concede
	//una posicion en el espacio. Esta es la funcion de un OBJECT, tambien conocidos como Entitys
	//en algunos sitios. Creamos el object con nuestro MESH (WiiMesh), nuestro MATERIAL (WiiMat)
	//y un par de vectores que indican posicion y orientacion en el espacio. El ultimo parametro es
	//un identificador, aun no es util, podeis poner un numero entero al azar.
	AddObj(mRoot, WiiObj);
	//Con AddObj a�adimos un objeto nuevo a la raiz, ningun objeto que creemos aparecera en escena
	//hasta que lo a�adamos a la raiz.
	
	IMAGE * Puntero = AbrirImagen("puntero.png");
	PANEL * panel1 = NewPanel(Puntero, 220, 240, 3, 1);
	AddPnl(mRoot, panel1);
	//Esto ya se vio en la demo de panels, es un buen ejemplo
	//de como funciona el engine, creas las cosas, las a�ades, y listo.
	
	CAMERA mCamera;//Creamos una camara
	mCamera.cam = (Vector){0.0, -100.0, 31.0};
	mCamera.look = (Vector){0.0, -99.0, 31.0};
	mCamera.up = (Vector){0.0, 0.0, 1.0};
	//Damos valores a sus 3 vectores base
	//Asi definimos su posicion y orientacion iniciales.
	
	while(1) {//Ahora el bucle principal.
		
		WPAD_ScanPads();
		
		if (WPAD_ButtonsDown(0) & WPAD_BUTTON_HOME) exit(0);//Salir si pulsamos home
		
		wiimote = WPAD_Data(0);
		if(wiimote->ir.valid)
		{
			posx = wiimote->ir.x; //asignamos la posicion x
			posy = wiimote->ir.y; //asignamos la posicion y
			panel1->x = posx;
			panel1->y = posy;
		}//Hasta aqui es como en la demo de panels
		
		////////////Actualizamos la posicion de la camara
		if(posy > 300)
		MoveCam(&mCamera, ((300.0 - (f32)posy)/180.0), 0.0, 0.0);
		if(posy < 180)
		MoveCam(&mCamera, ((180.0 - (f32)posy)/180.0), 0.0, 0.0);
		if(posx < 200)
		RotateCamByAxis(&mCamera, 0.0, 0.0, 1.0, ((200.0 - (f32)posx)/2000.0));
		if(posx > 440)
		RotateCamByAxis(&mCamera, 0.0, 0.0, 1.0, ((440.0 - (f32)posx)/2000.0));
		if(WPAD_ButtonsHeld(0) & WPAD_BUTTON_UP)
		MoveCam(&mCamera, 0.0, 0.0, 1.0);
		if(WPAD_ButtonsHeld(0) & WPAD_BUTTON_DOWN)
		MoveCam(&mCamera, 0.0, 0.0, -1.0);
		if(WPAD_ButtonsHeld(0) & WPAD_BUTTON_LEFT)
		MoveCam(&mCamera, 0.0, 1.0, 0.0);
		if(WPAD_ButtonsHeld(0) & WPAD_BUTTON_RIGHT)
		MoveCam(&mCamera, 0.0, -1.0, 0.0);
		///////////////
		
		REV_Process(*mRoot, &mCamera);//llamamos al engine para que haga el trabajo por nosotros.
	}
}
//Fin.
